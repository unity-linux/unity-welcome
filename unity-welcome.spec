%define debug_package %{nil}

Name:		unity-welcome
Version:	0.1.3
Release:	1%{?dist}
Summary:	The Unity-Linux Welcome App

License:	GPLv2
URL:		https://gitlab.com/unity-linux/golivecd
# Source gets created in gitlab build
Source0: unity-welcome-src.tar.xz

BuildRequires: %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}
BuildRequires: desktop-file-utils
#For now should work with Chrome as well
Requires:  chromium

%description
The Unity-Linux Welcome App

%prep
%setup -q -n %{name}

%build
mkdir bin
%gobuild -o bin/unity-welcome ./...

%install
install -d %{buildroot}/%{_bindir}
install -d %{buildroot}/etc/skel/.config/autostart
install -d %{buildroot}%{_datadir}/applications
install -d %{buildroot}/%{_datadir}/%{name}/data
install -p -m 755 bin/%{name} %{buildroot}%{_bindir}
install -p -m 644 unity-welcome.desktop %{buildroot}/etc/skel/.config/autostart/unity-welcome.desktop
install -p -m 644 unity-welcome.desktop %{buildroot}%{_datadir}/applications/unity-welcome.desktop
cp -rf data/* %{buildroot}%{_datadir}/%{name}/data/
chmod -R 0655 %{buildroot}%{_datadir}/%{name}/data

# validate desktop file
desktop-file-validate %{buildroot}%{_datadir}/applications/unity-welcome.desktop

%files
%doc README.md
%{_bindir}/%{name}
%dir %attr(755,root,root) %{_datadir}/%{name}
%{_datadir}/%{name}/data
%{_datadir}/applications/unity-welcome.desktop
/etc/skel/.config/autostart/unity-welcome.desktop


%changelog
