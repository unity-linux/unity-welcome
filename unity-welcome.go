package main

import (
        "net"
        "net/http"
        "runtime"
        "log"
        "os"
        "os/signal"
        "syscall"
        "github.com/zserge/lorca"
        "fmt"
        "os/exec"
	"os/user"
	"strings"
)

var ui lorca.UI
var err error

func main() {
	args := []string{}
	if runtime.GOOS == "linux" {
		args = append(args, "--class=Lorca --disable-resize")
	}
	ui, err = lorca.New("", "", 800, 480, args...)
	if err != nil {
		log.Fatal(err)
	}
	defer ui.Close()

	ln, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		log.Fatal(err)
	}
	defer ln.Close()
	var data_dir string
	if _, err := os.Stat("./data"); os.IsNotExist(err) {
		data_dir = "/usr/share/unity-welcome/data"
	} else {
		data_dir = "./data"
	}
	go http.Serve(ln, http.FileServer(http.Dir(data_dir)))
	ui.Load(fmt.Sprintf("http://%s", ln.Addr()))

        ui.Bind("quit", quit)
        ui.Bind("auto_off", auto_off)
        ui.Bind("discuss", func() { exec.Command("xdg-open", "https://discuss.unitylinux.org").Start() } )
	ui.Bind("gitlab", func() { exec.Command("xdg-open", "https://gitlab.com/unity-linux").Start() } )
	ui.Bind("install", func() { exec.Command("bash", "-c", "sudo /sbin/anaconda --liveinst --method=livecd:/dev/loop2").Start() } )
        ui.Bind("getDesktop", getDesktop)
        ui.Bind("launchHelp", launchHelp)

        if err != nil {
                panic(err)
        }

        sigc := make(chan os.Signal)
        signal.Notify(sigc, syscall.SIGINT, syscall.SIGTERM)
        select {
        case <-sigc:
        case <-ui.Done():
        }
        ui.Close()

}

func quit() {
        syscall.Kill(syscall.Getpid(), syscall.SIGTERM)
}

func getDesktop() {
	desktopEnv, err := exec.Command("bash", "-c", "echo $(basename $DESKTOP_SESSION)|tr -d '\n'").CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
        osRelease, err := exec.Command("bash", "-c", "grep PRETTY_NAME /etc/os-release |awk -F'\"' '{print $2}'|tr -d '\n'").CombinedOutput()
        if err != nil {
                log.Fatal(err)
        }
        arch, err := exec.Command("bash", "-c", "echo $(arch)").CombinedOutput()
        if err != nil {
                log.Fatal(err)
        }
	output :=  string(osRelease)+" / "+string(arch)+" / "+strings.ToUpper(string(desktopEnv))
	ui.Eval(fmt.Sprintf(`document.querySelector('#getDesktop').value = %q`, output))
	getHelp()
}

func getHelp() {
        desktopEnv, err := exec.Command("bash", "-c", "echo $(basename $DESKTOP_SESSION)|tr -d '\n'").CombinedOutput()
        if err != nil {
                log.Fatal(err)
        }
	desktopName := strings.Title(string(desktopEnv))
	output := ("<i class=\"fa fa-life-ring\"> "+desktopName+" Help</i>")
	ui.Eval(fmt.Sprintf(`document.querySelector('#getHelp').innerHTML = %q`, output))
}

func launchHelp() {
        desktopEnv, err := exec.Command("bash", "-c", "echo $(basename $DESKTOP_SESSION)|tr -d '\n'").CombinedOutput()
        if err != nil {
                log.Fatal(err)
        }
        desktopName := string(desktopEnv)
	if desktopName == "plasma" {
		exec.Command("khelpcenter").Start()
	} else if desktopName == "xfce" {
		exec.Command("xdg-open", "https://docs.xfce.org/start").Start()
	} else {
		fmt.Println("Not here yet")
        }
}

func auto_off() {
  usr, err := user.Current()
  if err != nil {
      log.Fatal( err )
  }
  desktop_file_path := (usr.HomeDir+"/.config/autostart/unity-welcome.desktop")
  if _, err := os.Stat(desktop_file_path); err == nil {
      os.Remove(desktop_file_path)
      } else {
	      fmt.Println("File: "+desktop_file_path+" does not exist")
  }
}
